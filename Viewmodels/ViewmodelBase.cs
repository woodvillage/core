﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Disposables;
using System.Runtime.CompilerServices;
using System.Text;

namespace Woodvillage.Core.Viewmodels
{
    /// <summary>
    /// Basic implementation of the INotifyPropertyChanged interface to be used as basis
    /// for all viewmodels. Offers automatic notification on the setter of a property, if
    /// property is implemented like:
    /// private int _myProperty;
    /// public int MyProperty
    /// { get => _myProperty; set => SetField(ref _myProperty, value); }
    /// </summary>
    public abstract class ViewmodelBase : INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Event to announce the change of a property. The properties name is in the
        /// EventArgs
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Set the referenced field with the value and anounce the change if the value
        /// is not equal to the old via the INotifyPropertyChanged interface
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="field">Reference to the corresponding field</param>
        /// <param name="value">New value</param>
        /// <param name="propertyName">Name of the property.</param>
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            return true;
        }

        /// <summary>
        /// Shortcut for the Notification
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        protected void NotifyPropertyChanged(string propertyName) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        /// <summary>
        /// When using observables in the Viewmodel, collect all disposables in this collection
        /// </summary>
        protected CompositeDisposable _disposables = new CompositeDisposable();

        /// <summary>
        /// Implementation of the IDisposable interface, freeing up all subscriptions
        /// </summary>
        public virtual void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
