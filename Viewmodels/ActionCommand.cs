﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Woodvillage.Core.Viewmodels
{
    /// <summary>
    /// Basic implementation for the ICommand interface allowing to call a lambda with a single
    /// parameter.
    /// </summary>
    /// <typeparam name="T">Type of the lambda functions parameter</typeparam>
    public class ActionCommand<T> : ICommand
    {
        private bool _canExecute = true;
        private Action<T> _action;
        /// <summary>
        /// Event to show the CanExecute property changed.
        /// </summary>
        public event EventHandler CanExecuteChanged;
        private void ChangeCanExecute(bool canExecute)
        {
            _canExecute = canExecute;
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Check wether the ICommand can currently be executed.
        /// </summary>
        public bool CanExecute(object parameter) => _canExecute;

        /// <summary>
        /// Execute the ICommand
        /// </summary>
        public async void Execute(object parameter)
        {
            ChangeCanExecute(false);
            await Task.Run(() => _action((T)parameter));
            ChangeCanExecute(true);
        }

        /// <summary>
        /// Constructor. Feed with the lambda to be called, when the ICommand is being
        /// executed.
        /// </summary>
        /// <param name="action">lambda</param>
        public ActionCommand(Action<T> action)
        {
            _action = action;
        }
    }

    /// <summary>
    /// Parameterless implementation of the ICommand interface for a single lambda.
    /// </summary>
    public class ActionCommand : ICommand
    {
        private bool _canExecute = true;
        private Action _action;

        /// <summary>
        /// Event to show the CanExecute property changed.
        /// </summary>
        public event EventHandler CanExecuteChanged;
        private void ChangeCanExecute(bool canExecute)
        {
            _canExecute = canExecute;
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Check wether the ICommand can currently be executed.
        /// </summary>
        public bool CanExecute(object parameter) => _canExecute;

        /// <summary>
        /// Execute the ICommand
        /// </summary>
        public async void Execute(object parameter)
        {
            ChangeCanExecute(false);
            await Task.Run(() => _action());
            ChangeCanExecute(true);
        }

        /// <summary>
        /// Constructor. Feed with the lambda to be called, when the ICommand is being
        /// executed.
        /// </summary>
        /// <param name="action">lambda</param>
        public ActionCommand(Action action)
        {
            _action = action;
        }
    }
}
